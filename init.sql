DROP DATABASE IF EXISTS thegame_main;
DROP DATABASE IF EXISTS thegame;

CREATE DATABASE thegame_main DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
CREATE DATABASE thegame DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

USE thegame_main;

CREATE TABLE banned  (
	username	varchar(30) NOT NULL,
	since   	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	sid     	char(36) NOT NULL,
	mac     	bigint(20) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY(username,sid,mac)
);


CREATE TABLE logs  (
	id        	SERIAL,
	created_at	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	username  	varchar(30) NULL,
	message   	text NULL
	);

CREATE TABLE users  (
	username  	varchar(30) NOT NULL,
	hash      	text NULL,
	created_at	timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	protected 	bigint(20) UNSIGNED NOT NULL DEFAULT '0',
	level     	int(10) UNSIGNED NULL,
	admin     	tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
	nomac     	tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
	lastlogid 	bigint(20) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY(username)
);


INSERT INTO users (`username`, `hash`, `created_at`, `protected`, `level`, `admin`, `nomac`, `lastlogid`)
  VALUES('admin', '$2b$12$rcZU33N0cuNzWe.5ZsWKQu2Qdnkb7xiRg9weN1h9t83HIRj.ENlXe', '2016-01-10 21:34:46.0', 0, 0, true, false, 0);

USE thegame;

CREATE TABLE coordinates  (
	info	text NULL
	);

INSERT INTO coordinates (`info`)
  VALUES('FOURTH POSITION ON THE DOOR : 38.9983005 -94.77850460000002');


GRANT ALL PRIVILEGES ON thegame_main.* TO 'thegameuser'@'localhost' IDENTIFIED BY 'thegamepassword';
GRANT SELECT ON thegame.* TO 'thegamesgli'@'localhost' identified by 'sqli';
FLUSH PRIVILEGES;
