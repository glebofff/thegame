from flask.ext.login import LoginManager
from flask.ext.mysqldb import MySQL
from flask.ext.bcrypt import Bcrypt

lm = LoginManager()
mysql = MySQL()
bcrypt = Bcrypt()

