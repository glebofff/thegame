# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, g, flash, session, redirect, url_for, jsonify
from flask.ext.login import login_required, login_user, logout_user, current_user as u
from wtforms.validators import ValidationError
from werkzeug.datastructures import MultiDict
from uuid import uuid4
from netaddr import EUI
from ansi2html import Ansi2HTMLConverter
import subprocess
import json
import MySQLdb

from extensions import *
import utils
from auth import User

from settings import Config
from forms import *

# creating app
app = Flask(__name__)
app.config.from_object(Config)

# initializing extensions
for ext in [mysql, lm, bcrypt]:
    ext.init_app(app)

lm.login_view = 'index'
# lm.login_message_category = 'warning'
lm.login_message = None

app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True


@lm.user_loader
def load_user(uid):
    return User.by_id(uid)


@app.before_request
def before_request():
    if request.endpoint != "static":
        g.username = request.cookies.get('username')
        g.mac = utils.getmac(request.remote_addr)
        g.uid = session['uid'] = session.get('uid', str(uuid4()))


def render_error(error):
    error_code = getattr(error, 'code', 500)
    return render_template('{0}.html'.format(error_code), error_code=error_code), error_code

for errcode in [401, 404, 500]:
    app.errorhandler(errcode)(render_error)


@app.route('/')
def index():
    """
    When the user first connects to the page it checks to see if they have ever connected before (cookie).
    On Go:
    If cookie is present, present the login page.
    If cookie is not present, present the REGISTRATION page
    """

    if u.is_authenticated:
        if u.admin:
            return redirect(url_for('admin'))
        return redirect(url_for('user'))

    return render_template('index.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    fucky = utils.is_banned(g.mac, g.uid) or utils.is_protected(g.mac)

    if request.method == 'GET':
        form = FMRegister()
        form.login.data = g.username
        form.usermac.data = g.mac
        if fucky:
            flash("SOMETHING IS FUCKY! Please disconnect, fix it and try again.", 'warning')
    elif request.method == 'POST':
        try:
            form = FMRegister(request.form)
            if not form.validate_on_submit() or fucky:
                raise ValidationError

            # banned == yes
            if form.banned.data.lower() == 'yes':
                # update the banned table
                utils.set_ban(
                    username=form.login.data,
                    mac=g.mac,
                    uid=g.uid
                )

                # call ‘banned.py’ script
                utils.safe_call(Config.BANNED_CMD)

                # display [banned] page
                response = redirect(url_for('banned'))
                response.set_cookie('username', form.login.data)
                return response

            # username already registered
            user = User.by_id(form.login.data)
            if user:
                form.login.errors.append("User already exists, try another username.")
                raise ValidationError

            # all ok
            user = User(
                password=form.passwd.data,
                username=form.login.data,
                level=2
            )
            if not user.store():
                flash('Some error occured, please try again.', 'danger')
                raise ValidationError

            # If no MacAddress: Display warning to contact Admin of Game
            # (If this happens, we need a way to enable NOMAC login for a specific user)
            if g.mac.value == 0:
                flash("You have successfully registered, but, you cannot proceed without mac address."
                      "Try to contact admin and login with your new login and password", "warning")
                utils.log(username=user.username, message='User registered without MAC-address')
                return redirect(url_for('login'))

            login_user(user, remember=True)
            response = redirect(url_for('protect'))
            response.set_cookie('username', user.username)
            utils.safe_call(Config.WELCOME_CMD)
            return response

        except ValidationError:
            pass

    return render_template('register.html', form=form, fucky=bool(fucky))


@app.route('/banned')
def banned():
    return render_template('banned.html')


@app.route('/protect', methods=['GET','POST'])
@utils.ensure_banned
@login_required
def protect():

    protected = {}  # dict of {mac: username} values to grayout other's protected macs in form

    if request.method == 'GET':
        cur = mysql.connection.cursor()
        cur.execute(
            """
            SELECT
                b.mac,
                u.username as protected
            FROM
                banned b
            LEFT JOIN
                users u on u.protected = b.mac
            WHERE
                b.username = %s
                and b.sid = %s
                and b.mac != 0
            ORDER BY
                since desc
            """,
            (u.username, g.uid)
        )
        res = cur.fetchall()

        current_listed = False
        form = FMProtect()
        choices = []
        choosen = None
        for row in res:
            (_mac, user) = row

            if user != u.username:
                protected[_mac] = user
            else:
                choosen = _mac

            mac = EUI(_mac, dialect=Config.MAC_DIALECT)

            if _mac == int(g.mac):
                current_listed = True
                if user == u.username:
                    # if current MAC address is listed, have that one selected
                    choosen = choosen or _mac  # and if different mac is not protected before

            choices.append((_mac, str(mac)))

        # If no banned MacAddresses: Display their current MAC Address
        if not current_listed and int(g.mac):
            choices.append((int(g.mac), str(g.mac)))
            choosen = choosen or g.mac.value

        form.protect.data = choosen

        # why should we display this page, if there is no mac address to protect?
        if not choices:
            return redirect(url_for('user'))

        form.protect.choices = choices
        session['choices'] = choices
        session['protected'] = protected

    if request.method == 'POST':
        form = FMProtect(request.form)
        protected = session.get('protected', {})
        form.protect.choices = session.get('choices', [])
        if form.validate_on_submit():
            u.protected = form.protect.data
            u.store()
            for key in ['choices', 'protected']:
                session.pop(key)
            # TODO: call 'protect.py'
            # redirect to user page
            utils.safe_call(Config.PROTECT_CMD)
            return redirect(url_for('user'))

    res = render_template('protect.html', form=form, protected=protected)
    return res


@app.route('/login', methods=['GET', 'POST'])
@utils.ensure_banned
def login():

    warnmessage = """Disconnect and reconnect using your protected Mac Address.
                If you do not know what it is, contact Admin"""
    next = request.args.get("next", url_for('user'))
    if request.method == 'GET':
        form = FMLogin()
        form.login.data = g.username

    elif request.method == 'POST':
        form = FMLogin(request.form)
        if form.validate_on_submit():
            try:
                u = User.by_id(form.login.data)
                if not u:
                    raise KeyError
                if not u.check_password(form.passwd.data):
                    raise ValueError

                if u.admin:
                    next = url_for('admin')

                # current mac != 0
                elif int(g.mac) != 0:
                    # if user doesn't have protected mac in db:
                    if int(u.protected) == 0:
                        next = url_for('protect')

                    # If Client Mac Address = Protected Mac
                    elif u.protected == g.mac:
                        next = url_for('user')

                    else:
                        return render_template('warning.html', message=warnmessage)

                # current mac == 0
                elif int(g.mac) == 0:
                    if u.nomac:
                        next = url_for('user')

                    # no info about user's mac, and NOMAC is not set
                    else:
                        return render_template('warning.html', message=warnmessage)

                login_user(u, remember=True)

                response = redirect(next)
                response.set_cookie('username', u.username)
                return response

            except (KeyError, ValueError) as e:
                form.login.errors.append('No such username or password is wrong.')
                form.passwd.errors.append('')

    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    response = redirect('/')
    # response.delete_cookie('username')
    return response


@app.route('/ponyframe')
@utils.ensure_banned
@login_required
def ponyframe():
    """
    executes PONY_COMMAND, piped with ansi2html, results should be displayed in L3 iframe

    :return:
    """
    try:
        text = request.values.get('text')
        cmd = '%s %s' % (app.config.get('PONY_COMMAND', 'ponysay'), text)

        said = subprocess.check_output(cmd,
                                       shell=True,
                                       cwd=Config.PONY_CWD
                                       )
        conv = Ansi2HTMLConverter()
        html = conv.convert(said.decode("utf-8"))
    except subprocess.CalledProcessError as e:
        return "command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)
    return html


@app.route('/user', methods=['GET', 'POST'])
@utils.ensure_banned
@login_required
def user():
    g.stats = utils.stats()
    # L2 ###############################################################################################################
    if u.level == 2:
        userinput = request.form.get('input', '')
        if userinput == Config.HARDCODED:
            u.level = 3
            u.store()
            return redirect(request.path)

        tables = utils.mysqlcli(userinput)
        return render_template('user_l2.html', tables=tables)

    # L3 ###############################################################################################################
    if u.level == 3:
        return render_template('user_l3.html')

    return redirect(url_for('profile'))


@app.route('/profile', methods=['GET', 'POST'])
@utils.ensure_banned
@login_required
def profile():
    if request.method == 'GET':
        fmpasswd = FMPassword()
    elif request.method == 'POST':
        try:
            fmpasswd = FMPassword(request.form)

            if fmpasswd.validate_on_submit():
                if not u.check_password(fmpasswd.oldpass.data):
                    fmpasswd.oldpass.errors.append('Wrong password.')
                    raise ValueError

                u.hash = fmpasswd.passwd.data
                u.store()
                return redirect(url_for('user'))

        except ValueError:
            pass

    return render_template('profile.html', form=fmpasswd)


@app.route('/admin', methods=['GET', 'POST'])
@utils.ensure_banned
@login_required
def admin():
    if not u.admin:
        return redirect(url_for('user'))

    username = request.values.get('username')
    user = None
    fmuser = None
    fmsearch = FMUserSearch()
    fmsearch.username.data = username
    try:
        if username:
            user = User.by_id(username)
            if not user:
                fmsearch.username.errors = list(fmsearch.username.errors)
                fmsearch.username.errors.append(u'User %s not found' % username)
                raise KeyError
        else:
            raise KeyError

        if request.method == 'POST':
            fmuser = FMUserEdit(request.form)
            if not fmuser.validate_on_submit():
                raise ValidationError

            user.nomac = int('nomac' in request.form)
            user.admin = int('admin' in request.form)
            user.level = request.form.get('level', user.level)
            password = request.form.get('password')
            if password and not user.check_password(password):
                user.hash = password

            if user.store():
                flash('User %s updated.' % user.username, 'success' )

        else:
            fmuser = FMUserEdit(MultiDict(user.as_dict()))
    except (KeyError, ValidationError):
        pass

    return render_template('admin.html', fmsearch=fmsearch, fmuser=fmuser, user=user)


@app.route('/home')
@utils.ensure_banned
@login_required
def home():
    """
    Placeholder route

    :return: redirect to proper home
    """
    if u.admin:
        return redirect(url_for('admin'))
    return redirect(url_for('user'))


@app.route('/logcount')
@utils.ensure_banned
@login_required
def logcount():
    """
    Return count of unread log messages (json).
    :return:
    """
    count = 0
    if not u.admin:
        return json.dumps(count)

    try:
        cur = mysql.connection.cursor()
        res = cur.execute(
            """
            select count(*) from logs where id > %s
            """,
            (u.lastlogid,)
        )
        if res:
            count = cur.fetchone()[0] or 0

    except (MySQLdb.DatabaseError, IndexError):
        pass

    return json.dumps(count)


@app.route('/logmessages')
@utils.ensure_banned
@login_required
def logmessages():
    if not u.admin:
        return redirect(url_for('home'))

    lastlogid = u.lastlogid
    rows = []
    try:
        cur = mysql.connection.cursor()
        cur.execute(
            """
            select * from logs where id > (%s - 10)
            order by id desc
            limit 100
            """,
            (u.lastlogid, )
        )

        rows = cur.fetchall()
        if rows:
            u.lastlogid = int(rows[0][0])
            u.store()

    except (MySQLdb.DatabaseError, IndexError):
        pass

    return render_template('messages.html', lastlogid=lastlogid, rows=rows)


if __name__ == '__main__':
    app.run(
        debug=app.config.get("DEBUG", "true"),
        host=app.config.get("HOST", "localhost"),
        port=app.config.get("PORT", 5000)
    )
