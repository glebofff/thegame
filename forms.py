from flask_wtf import Form
from netaddr import valid_mac

import wtforms.validators as validators
from wtforms import StringField, PasswordField, HiddenField, RadioField, BooleanField, IntegerField
from wtforms.widgets import PasswordInput


mmismatch = "Passwords mismatch."
mwrong = "Wrong password."
mmacwrong = "Mac address is not valid."


def vvalid_mac(form, field):
    if not valid_mac(field.data):
        raise ValueError(mmacwrong)


class FMLogin(Form):
    login = StringField(u"Username", [validators.InputRequired()])

    passwd = StringField(u"Password",
                         [
                             validators.InputRequired(),
                         ],
                         widget=PasswordInput(hide_value=False),
                         default=''
                         )


class FMRegister(Form):
    login = StringField(u"Username", [
        validators.InputRequired()
    ], description="""* please note: You only get one shot at creating a username,
    please choose the one that you want to use for the rest of the game."""
                        )

    passwd = StringField(u"Password",
                         [
                             validators.InputRequired(),
                             validators.EqualTo('passverify', message=mmismatch)
                         ],
                         widget=PasswordInput(hide_value=False),
                         default='')

    passverify = PasswordField(u'Verify password',
                               [
                                   validators.InputRequired(),
                                   validators.EqualTo('passwd', message=mmismatch)
                               ],
                               widget=PasswordInput(hide_value=False),
                               default='')

    banned = HiddenField(default='yes')

    usermac = StringField(u"Detected MAC address", [validators.Optional()])


class FMProtect(Form):
    protect = RadioField([validators.InputRequired()], choices=[], coerce=int)


class FMUserSearch(Form):
    username = StringField(u'User name', [validators.Optional()], default='')


class FMUserEdit(Form):
    password = StringField(u"Password",
                         [
                             validators.Optional(),
                         ],
                         )

    #passverify = PasswordField(u'Verify password',
    #                           [
    #                               validators.Optional(),
    #                               validators.EqualTo('password', message=mmismatch)
    #                           ],
    #                           widget=PasswordInput(hide_value=False),
    #                           default='')

    protected = StringField(u"Protected", [validators.Optional(),
                                           vvalid_mac
                                           ])
    nomac = BooleanField(u"No mac address",
                         false_values=[0, False, None, ''])
    admin = BooleanField(u"Set as Admin",
                         false_values=[0, False, None, ''])
    level = IntegerField(u"Level", [validators.Optional()])


class FMPassword(Form):
    oldpass = StringField(u"Old password",
                         [
                             validators.InputRequired(),
                         ],
                         widget=PasswordInput(hide_value=False),
                         default='')

    passwd = StringField(u"New Password",
                         [
                             validators.InputRequired(),
                             validators.EqualTo('passverify', message=mmismatch)
                         ],
                         widget=PasswordInput(hide_value=False),
                         default='')

    passverify = PasswordField(u'Verify password',
                               [
                                   validators.InputRequired(),
                                   validators.EqualTo('passwd', message=mmismatch)
                               ],
                               widget=PasswordInput(hide_value=False),
                               default='')
