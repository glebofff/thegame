$(document).ready(function() {
    var logcount = undefined,
        msgtimeout = 0,
        el = $('#logcount'),

        checkmessages = function() {
            clearTimeout(msgtimeout);
            $.getJSON(
                '/logcount',
                function(msg) {
                    showtip(msg)
                }
            ).always( function() {
                msgtimeout = setTimeout(
                    checkmessages,
                    2000
                )
            })
        },

        showtip = function(msg) {

            if (msg === 0) {
                el.addClass('hidden');
                el.hide();
                logcount = 0;
                return;
            }
            if (!msg) {
                return;
            }

            if (logcount != msg) {
                el.html(msg);
                el.removeClass('hidden');
                el.show();
                el.tooltip(
                    {
                        placement: 'bottom',
                        title: 'You have unread log messages.'
                    }
                ).tooltip('show');
                logcount = msg;
            }

        };

    checkmessages();
});