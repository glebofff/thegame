#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Management script."""
import os
from glob import glob
from subprocess import call

from flask_script import Command, Manager, Option, Server, Shell
from flask_script.commands import Clean, ShowUrls

from settings import Config
from extensions import mysql as db, bcrypt
from thegameapp import app
from auth import User

PWD = os.path.abspath(os.path.dirname(__file__))

manager = Manager(app)


def _make_context():
    """Return context dict for a shell session so you can access app, db, and the User model by default."""
    return {'app': app, 'db': db}


@manager.command
def genhash(password):
    hash = bcrypt.generate_password_hash(password)
    print hash.decode('utf-8')


@manager.command
def updateuser(username, password, level, admin, nomac, protected, lastlogid):
    u = User(password=password,
             **{'username': username,
                'level': level,
                'admin': admin,
                'nomac': nomac,
                'protected': protected,
                'lastlogid': lastlogid
                })
    if u.store():
        print u
    else:
        print 'shit happens'

manager.add_command('runserver', Server(host=Config.HOST, port=Config.PORT))
manager.add_command('shell', Shell(make_context=_make_context))
manager.add_command('urls', ShowUrls())
manager.add_command('clean', Clean())

if __name__ == '__main__':
    manager.run()
