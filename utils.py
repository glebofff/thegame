import subprocess
from functools import wraps

from MySQLdb import ProgrammingError, MySQLError, escape_string, DatabaseError
from MySQLdb.cursors import DictCursor
from flask import g, redirect, url_for
from netaddr import EUI, valid_ipv4, ip as IP

from extensions import mysql
from settings import Config

_protected = {}
_banned = {}

zeromac = EUI(0, dialect=Config.MAC_DIALECT)


def getmac(ipaddr):
    """
    Tries to obtain and return mac-address of ipaddr.
    :param ipaddr:
    :return:
    """
    res = zeromac
    if not valid_ipv4(ipaddr):
        return res

    try:
        with open('/proc/net/arp') as f:
            for l in f:
                if l.startswith('IP'):
                    continue

                (ip, _, _, mac, _, _) = l.split()
                if IP.IPAddress(ip) == IP.IPAddress(ipaddr):
                    res = EUI(mac, dialect=Config.MAC_DIALECT)
                    break
    except OSError:
        pass

    return res


def is_protected(addr):
    """
    Checks if addr is protected
    :param addr:
    :return:
    """
    macaddr = int(addr)

    # zero mac address can't be protected
    if macaddr == 0:
        return False

    try:
        if Config.FORCE_DB:
            raise KeyError
        return _protected[macaddr]
    except KeyError:
        cur = mysql.connection.cursor()
        cur.execute(
            """
            select username from users where protected=%s limit 1
            """,
            (macaddr,)
            )
        row = cur.fetchone()
        if row:
            _protected[macaddr] = row[0]
        else:
            _protected[macaddr] = None
        return _protected[macaddr]


def is_banned(addr, uid):
    """
    Checks if current mac address is banned.
    :param addr:
    :param uid:
    :return:
    """
    macaddr = int(addr)
    try:
        if Config.FORCE_DB:
            raise KeyError
        return _banned[macaddr]
    except KeyError:
        cur = mysql.connection.cursor()
        cur.execute(
            """
            select username from banned where mac=%s and sid=%s limit 1
            """,
            (macaddr, uid,)
        )
        row = cur.fetchone()
        if row:
            _banned[macaddr] = row[0]
        else:
            _banned[macaddr] = None
        return _banned[macaddr]


def set_ban(username=None, uid=None, mac=zeromac):
    """
    Sets ban on specified username with uid (internal session variable)
    :param username:
    :param uid:
    :param mac:
    :return:
    """
    if not username:
        return False
    macaddr = int(mac)
    try:
        cur = mysql.connection.cursor()
        cur.execute(
            """
            INSERT INTO banned(username, sid, mac)
            VALUES(%s, %s, %s)
            """,
            (
                escape_string(username),
                escape_string(uid),
                macaddr
            )
        )
        mysql.connection.commit()
        _banned[macaddr] = username
        return True

    except DatabaseError:
        return False


def ensure_banned(func):
    """
    Wrapper, redirects banned users to banned page.
    """
    @wraps(func)
    def wrapped(*args, **kwargs):
        if is_banned(g.mac, g.uid):
            return redirect(url_for('banned'))
        return func(*args, **kwargs)

    return wrapped


def stats():
    """
    Execute query and return dictionary with game statistics {'banned': x, 'users': y}
    :return:
    """
    try:
        cur = mysql.connection.cursor(cursorclass=DictCursor)
        cur.execute("""select (select count(*) from banned) as banned,
       (select count(*) from users) as users""")
        row = cur.fetchone()
        return row
    except DatabaseError:
        return None


def log(username=None, message=None):
    """
    Insert new record in logs table.

    :param username:
    :param message:
    :return: boolean result of operation
    """
    try:
        cur = mysql.connection.cursor(cursorclass=DictCursor)
        res = cur.execute(
            """
            insert into logs (username, message)
            values (%s, %s)
            """,
            (username, message)
        )
        mysql.connection.commit()
        if res:
            return True

    except DatabaseError:
        pass

    return False


def safe_call(*args, **kwargs):
    try:
        subprocess.call(*args, **kwargs)
    except OSError:
        pass


def mysqlcli(string):
    cmd = 'mysql -v -B -h%s -u%s -p%s %s' % (Config.MYSQL_HOST, Config.SQLI_USER, Config.SQLI_PASSWORD, Config.SQLI_DB)
    query = subprocess.Popen(
        cmd.split(),
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        close_fds=True
    )
    (out, err) = query.communicate(Config.SQLI_QUERY % string or '')
    results = []
    result = []

    def flush():
        if result:
            results.append(result[:])
            result[:] = []

    lines = iter(out.splitlines())
    for line in lines:
        if line.startswith('-'):
            2*next(lines, None)
            flush()

        else:
            result.append(line)
    flush()

    return results





