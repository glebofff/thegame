import os
from netaddr import mac_unix_expanded


class Config:
    SECRET_KEY = os.environ.get('THEGAME_SECRET', 'H\xef\xb0Z\x19M4\xb655&\x18c\x89\x8e\x8d\x07k\x04\xa5\xd3\x89\x026')
    SESSION_PROTECTION = 'strong'

    # main database
    MYSQL_HOST = 'localhost'
    MYSQL_DB = 'thegame_main'
    MYSQL_USE_UNICODE = True
    MYSQL_USER = 'thegameuser'
    MYSQL_PASSWORD = 'thegamepassword'
    MYSQL_EXECUTABLE = 'mysql'

    # l2 database for sqli
    SQLI_DB = 'thegame'
    SQLI_USER = 'thegamesqli'
    SQLI_PASSWORD = 'sqli'
    SQLI_QUERY = "select 'Hi there!%s'"  # where %s is placeholder for user input

    # app settings
    HOST = "0.0.0.0"
    PORT = 5000
    DEBUG = True
    FORCE_DB = True     # force DB queries instead of using cache dictionaries
    MAC_DIALECT = mac_unix_expanded

    # pony settings
    PONY_COMMAND = "python3 /usr/bin/ponysay"
    PONY_CWD = "/tmp"

    # commands to execute on related pages
    WELCOME_CMD = 'welcome.py'
    BANNED_CMD = 'banned.py'
    PROTECT_CMD = 'protect.py'

    # hardcoded answer on L2 page
    HARDCODED = u'hardcoded'
