# the game #

## installation ##

To init database, run from superuser:
`mysql < init.sql`

Python requirements are listed in the `requirements.txt` file:
`pip install -r requirements.txt`

## run application ##

Run from command line: `python thegameapp.py` or `python manage.py runserver`