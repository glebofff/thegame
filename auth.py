from MySQLdb import ProgrammingError, MySQLError, escape_string
from MySQLdb.cursors import DictCursor
from extensions import mysql, bcrypt
from flask.ext.login import UserMixin

from netaddr import EUI
from settings import Config


class User(UserMixin):
    _byid = {}

    def __init__(self, password=None,
                 username=None,
                 created_at=None,
                 level=0, admin=0,
                 nomac=0,
                 hash=None,
                 protected=0,
                 lastlogid=0):
        self._hash = hash
        self.username = username
        self.admin = admin
        self.nomac = nomac
        self.level = level
        self.lastlogid = lastlogid
        self.created_at = created_at
        self.protected = EUI(protected, dialect=Config.MAC_DIALECT)

        if password:
            self.hash = password

    def get_id(self):
        return self.username

    def check_password(self, password):
        if self.hash:
            return bcrypt.check_password_hash(self.hash, password)
        return False

    @classmethod
    def setcache(cls, user):
        cls._byid[user.username] = user

    @staticmethod
    def by_id(uid):
        """
        Factory method, generates User object from SQL database
        :param uid:
        :return: User or None
        """
        try:
            if Config.FORCE_DB:
                raise KeyError
            return User._byid[uid]
        except KeyError:
            cur = mysql.connection.cursor(cursorclass=DictCursor)
            cur.execute("""
                select *
                from
                  users
                where
                  username = %s""",
                        (escape_string(uid),))
            res = cur.fetchone()
            if res:
                # names = [s[0] for s in cur.description]
                # args = dict(zip(names, res))
                user = User(**res)
                User.setcache(user)
                return user
        return None

    def as_escaped_dict(self):
        return {
            'username': escape_string(self.username),
            'hash': escape_string(self.hash),
            'created_at': self.created_at,
            'level': self.level,
            'admin': self.admin,
            'nomac': self.nomac,
            'protected': int(self.protected),
            'lastlogid': int(self.lastlogid)
        }

    def as_dict (self):
        return {
            'username': self.username,
            'hash': self.hash,
            'created_at': self.created_at,
            'level': self.level,
            'admin': self.admin,
            'nomac': self.nomac,
            'protected': self.protected,
            'lastlogid': self.lastlogid
        }

    def store(self):
        """
        Store new user or update one.
        """
        cur = mysql.connection.cursor()
        try:
            res = cur.execute(
                """
                INSERT INTO users(username, hash, created_at, protected, level, admin, nomac, lastlogid)
                VALUES(%(username)s, %(hash)s, %(created_at)s, %(protected)s, %(level)s, %(admin)s, %(nomac)s, %(lastlogid)s)
                ON DUPLICATE KEY UPDATE
                hash = %(hash)s,
                level = %(level)s,
                protected = %(protected)s,
                admin = %(admin)s,
                nomac = %(nomac)s,
                lastlogid = %(lastlogid)s
                """,
                self.as_escaped_dict()
            )
            mysql.connection.commit()
            print res
            if res:
                return True
            return False
        except (MySQLError, ProgrammingError):
            return False

    @property
    def hash(self):
        return self._hash

    @hash.setter
    def hash(self, val):
        if val is None:
            self._hash = None
        else:
            self._hash = bcrypt.generate_password_hash(val).decode('utf-8')

    def __repr__(self):
        return '<User %r>' % self.username
